---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Instalar dependencias de desarrollo
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalar dependencias de desarrollo

## Instala `git` en tu máquina

Necesitas tener la herramienta `git` instalada para hacer uso de los repositorios

```bash
# apt install git
```

!!! note
    - Si utilizas **macOS**, puedes instalar mediante [`brew install git`][brew-git]
    - Si utilizas Windows, puedes instalar via [`choco install git`][choco-git] o [`winget install git`][winget]

### Instalación de `pre-commit`

El programa `pre-commit` está escrito en Python y adicionalmente son necesarias las dependencias de desarrollo para el manejador de paquetes de Python (`pip3`).

```
# apt install build-essential python3-pip
```

Verifica que tengas `pip3` instalado y agrega una ruta para alterar la variable de entorno `PATH` para tu usuario.

```bash
$ which pip3
/usr/bin/pip3

$ echo 'PATH=${HOME}/.local/bin:${PATH}' >> ~/.bashrc
```

!!! warning
    Debes de realizar alguna de las siguientes acciones después de modificar el archivo `~/.bashrc`:

    - Ejecuta `source ~/.bashrc` en la terminal actual para incluir los cambios
    - Abre otra ventana de la terminal para que se lea el nuevo contenido del archivo `~/.bashrc`
    - Cierra y abre tu sesión para que reconozca los cambios
    - Reinicia tu máquina

Una vez que tengas el programa `pip3` en tu sistema, instala `pre-commit` en tu perfil de usuario.

```bash
$ pip3 install --user pre-commit
Collecting pre-commit
	...
Installing collected packages: ... pre-commit
	...
Successfully installed ... pre-commit-2.20.0 ...
```

Verifica que el _shell_ reconozca la ubicación de `pre-commit`

```bash
$ which pre-commit
/home/tonejito/.local/bin/pre-commit
```

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] si ya tienes las herramientas instaladas

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
|                             | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../../workflow
[arriba]: ../../workflow
[siguiente]: ../crear-fork

[brew]: https://brew.sh/#install
[brew-git]: https://formulae.brew.sh/formula/git#default
[choco]: https://chocolatey.org/install
[choco-git]: https://community.chocolatey.org/packages?q=git
[winget]: https://docs.microsoft.com/en-us/windows/package-manager/winget/
[winget-git]: https://github.com/microsoft/winget-pkgs/tree/master/manifests/g/Git/Git
