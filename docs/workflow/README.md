---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Flujo de trabajo para la entrega de actividades
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Flujo de trabajo para la entrega de actividades

| ![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")
|:--------:|
|

<!-- -->
!!! note
    Este contenido también está disponible en video:

    - [Flujo de trabajo para entrega de actividades 📼](https://youtu.be/GpR8znNagGo&t=0&list=PLN1TFzSBXi3QWbHwBEV3p4LxV5KceXu8d&index=2)
<!-- -->

El flujo de trabajo consiste en los siguientes pasos:

- [Instalar dependencias](instalar-dependencias)
- [Hacer _fork_ del repositorio principal de actividades](crear-fork)
- [Clonar el repositorio y crear rama de trabajo](clonar-fork)
- [Instalar pre-commit en el repositorio local](configurar-precommit)
- [Agregar contenido a la copia de trabajo y enviar los cambios](agregar-contenido)
- [Crear el _merge request_](crear-merge-request)

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-1/tareas-redes.git
[repositorio-vista-web]: https://redes-ciencias-unam.gitlab.io/2023-1/tareas-redes/
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-redes
