---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Agregar contenido a tu fork del repositorio
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Agregar contenido a tu _fork_ del repositorio

## Agregar carpeta personal

- Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `docs/tareas`

```bash
$ mkdir -vp docs/tareas/tarea-0/AndresHernandez/
mkdir: created directory ‘docs/tareas/tarea-0/AndresHernandez/’
```

!!! warning
    No utilices **espacios**, **acentos**, **eñe**, **diéresis** o **caracteres especiales**

!!! note
    - Dentro de esa carpeta es donde debes poner los archivos de esta tarea
    - Cada práctica y tarea tendrá su propia carpeta y debes seguir la estructura de directorios propuesta

--------------------------------------------------------------------------------

### Agregar una imagen

- Crea el directorio de imagenes

```bash
$ mkdir -vp docs/tareas/tarea-0/AndresHernandez/img/
mkdir: created directory ‘docs/tareas/tarea-0/AndresHernandez/img/’

$ touch docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
```

!!! note
    Se crea el archivo oculto `.gitkeep` para versionar un directorio _"vacío"_, porque `git` no permite versionar directorios

- Copia una imagen en formato `jpg`, `png` o `gif` dentro del directorio `img`

!!! warning
    - Asegúrate de que la imagen no pese más de `100 KB`

```bash
$ cp  /ruta/hacia/tonejito.png  docs/tareas/tarea-0/AndresHernandez/img/
$ ls -lA docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
-rw-r--r-- 1 tonejito staff 65535 Sep 28 00:10 docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

!!! note
    El nombre de tu imagen segúramente es diferente, pero debe estar dentro de la capeta `img`

--------------------------------------------------------------------------------

### Agregar archivo con tu nombre

- Crea el archivo `.gitkeep` y un archivo `README.md` con el contenido de muestra
    - Agrega una referencia a la imagen que incluiste en el paso anterior para que se muestre dentro del archivo `README.md`
    - Puedes agregar otros campos si gustas

```bash
$ touch  docs/tareas/tarea-0/AndresHernandez/README.md
$ editor docs/tareas/tarea-0/AndresHernandez/README.md
```

!!! note
    Reemplaza el comando `editor` con el editor de texto de tu preferencia

    - Puedes utilizar el [IDE de GitLab][gitlab-ide-live-preview] que cuenta con una función de [previsualización de Markdown][gitlab-markdown-live-preview]
    - También puedes visualizar cómo se convierte de Markdown a HTML en algún sitio como [stackedit.io][stackedit]

- Contenido de ejemplo para el archivo `README.md`
    - Este archivo contiene un título, algunas listas, una liga a una URL externa y una referencia a una imágen

```bash
# Andrés Hernández

- Número de cuenta: `123456789`

Hola, esta es mi carpeta para la [*tarea-0*](https://redes-ciencias-unam.gitlab.io/2023-1/tareas-redes/entrega/tarea-0).

Actividades a las que me quiero dedicar al salir de la facultad:

- Seguridad informática (red team, blue team)
- DevOps (Kubernetes)
- Desarrollo

Cosas que me gusta hacer en mi tiempo libre:

- Dormir
- Jugar videojuegos
- Ver películas
- Hacer experimentos con componentes electrónicos

| ![](img/tonejito.png)       |
|:---------------------------:|
| Esta es la imagen que elegí |
```

!!! note
    El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`


--------------------------------------------------------------------------------

## Envía los cambios a tu repositorio

- Una vez que hayas creado los archivos, revisa el estado del repositorio

```bash
$ git status
On branch AndresHernandez
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	docs/tareas/tarea-0/AndresHernandez/

nothing added to commit but untracked files present (use "git add" to track)
```

- Arega los archivos de tu carpeta con `git add`

```bash
$ git add docs/tareas/tarea-0/AndresHernandez/
```

- Revisa que los archivos hayan sido agregados al _staging area_ utilizando `git status`

```bash
$ git status
On branch AndresHernandez
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   docs/tareas/tarea-0/AndresHernandez/README.md
	new file:   docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
	new file:   docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

- Versiona los archivos con `git commit`

!!! warning
    - Utiliza **comillas simples** para especificar el mensaje del _commit_
    - Evita utilizar comillas dobles porque el _shell_ interpreta algunos caracteres como `*`, <code>&#96;</code>, etc.

```bash
$ git commit -m 'Carpeta de Andrés Hernández'
[AndresHernandez cb7fe99] Carpeta de Andrés Hernández
 3 files changed, 22 insertions(+)
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/README.md
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

- Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```bash
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-redes.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-redes.git (push)
```

!!! danger
    Si la salida del comando anterior **no apunta a tu _fork_ del repositorio**, tendras que borrar el directorio y volver a repetir los pasos desde la sección "[clona el repositorio](#clona-el-repositorio)"

- Revisa la rama en la que estas para enviarla a GitLab

```bash
$ git branch
* AndresHernandez
  entregados
  entregas
  main
```

- Envía los cambios a **tu repositorio** utilizando `git push`

```bash
$ git push -u origin AndresHernandez
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 12 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (10/10), 63.63 KiB | 1.0 MiB/s, done.
Total 10 (delta 1), reused 1 (delta 0), pack-reused 0
remote:
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote:
To https://gitlab.com/USUARIO/tareas-redes.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```


--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando hayas agregado el contenido a tu _fork_ y enviado los cambios a tu repositorio en GitLab

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../configurar-precommit
[arriba]: ../../workflow
[siguiente]: ../crear-merge-request


[gitlab-ide-live-preview]: https://docs.gitlab.com/ee/user/project/web_ide/#live-preview
[gitlab-markdown-live-preview]: https://about.gitlab.com/blog/2021/09/21/introducing-markdown-live-preview/
[stackedit]: https://stackedit.io/app#
